       IDENTIFICATION DIVISION.
       PROGRAM-ID. PROGCOB07.
      *********************************
      * AREA DE COMENTARIOS - REMARKS
      * AUTHOR = Marcelo
      * DATA = 15/11/2019
      * OBJETIVO = RECEBER 02 NOTAS, MEDIA E IMPRIMIR
      * UTILIZAR COMANDOS IF/ELSE/ENDIF
      *********************************
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       77 WRK-NOTA1     PIC 9(2)    VALUE ZEROS.
       77 WRK-NOTA2     PIC 9(2)    VALUE ZEROS.
       77 WRK-MEDIA     PIC 9(2)V99 VALUE ZEROS.
       77 WRK-MEDIA-ED  PIC Z9,99   VALUE ZEROS.
       PROCEDURE DIVISION.
           ACCEPT WRK-NOTA1.
           ACCEPT WRK-NOTA2.
           COMPUTE WRK-MEDIA = (WRK-NOTA1 + WRK-NOTA2) / 2.
           MOVE WRK-MEDIA TO WRK-MEDIA-ED.
           DISPLAY 'MEDIA ' WRK-MEDIA-ED.

           IF WRK-MEDIA >= 6,00
             DISPLAY 'APROVADO'
           ELSE
             IF WRK-MEDIA >= 2,00
               DISPLAY 'RECUPERACAO'
             ELSE
               DISPLAY 'REPROVADO'
             END-IF
           END-IF.

           STOP RUN.
