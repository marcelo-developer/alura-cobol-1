       IDENTIFICATION DIVISION.
       PROGRAM-ID. PROGCOB06.
      *********************************
      * AREA DE COMENTARIOS - REMARKS
      * AUTHOR = Marcelo
      * DATA = 11/11/2019
      * OBJETIVO = USO DO SINAL
      *********************************
       ENVIRONMENT DIVISION.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       77 WRK-NUM1     PIC 9(2)  VALUE ZEROS.
       77 WRK-NUM2     PIC 9(2)  VALUE ZEROS.
       77 WRK-RESUL    PIC S9(3) VALUE ZEROS.
       77 WRK-RESUL-ED PIC -ZZ9  VALUE ZEROS.
       PROCEDURE DIVISION.
           ACCEPT WRK-NUM1 FROM CONSOLE.
           ACCEPT WRK-NUM2 FROM CONSOLE.
           DISPLAY '==================================='.
           DISPLAY 'NUMERO1.. ' WRK-NUM1.
           DISPLAY 'NUMERO2.. ' WRK-NUM2.
      ************** SUBTRACAO
           SUBTRACT WRK-NUM2 FROM WRK-NUM1 GIVING WRK-RESUL.
           MOVE WRK-RESUL TO WRK-RESUL-ED.
           DISPLAY 'SUBSTRACAO ..... ' WRK-RESUL.
           DISPLAY 'SUBSTRACAO ..... ' WRK-RESUL-ED.
           STOP RUN.
