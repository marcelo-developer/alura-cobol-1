       IDENTIFICATION DIVISION.
       PROGRAM-ID. AUMENTOSALARIO.
      *********************************
      * AREA DE COMENTARIOS - REMARKS
      * AUTHOR = Marcelo
      * DATA = 06/11/2019
      * OBJETIVO = CALCULAR AUMENTO SALARIO EM FUNCAO DO TEMPO DA CASA
      * CALCULAR AREA
      *********************************
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       77 WRK-NOME         PIC X(20)    VALUE SPACES.
       01 WRK-ANO-ENTRADA  PIC 9(04)    VALUE ZEROS.
       77 WRK-SALARIO      PIC 9(05)V99 VALUE ZEROS.
       77 WRK-AUMENTO      PIC 9(06)V99 VALUE ZEROS.
       01 WRK-TEMPO-ANOS.
         02 WRK-ANO        PIC 9(04)    VALUE ZEROS.
         02 WRK-MES        PIC 9(02)    VALUE ZEROS.
         02 WRK-DIA        PIC 9(02)    VALUE ZEROS.
       PROCEDURE DIVISION.
           DISPLAY 'NOME.. '
           ACCEPT WRK-NOME.

           DISPLAY 'ANO DE ENTRADA.. '
           ACCEPT WRK-ANO-ENTRADA.

           DISPLAY 'SALARIO.. '
           ACCEPT WRK-SALARIO.

           ACCEPT WRK-TEMPO-ANOS FROM DATE YYYYMMDD.
           SUBTRACT WRK-ANO-ENTRADA FROM WRK-ANO GIVING WRK-ANO.

           EVALUATE WRK-ANO
             WHEN 0 THRU 2
               MOVE 1,00 TO WRK-AUMENTO
             WHEN 2 THRU 5
               MOVE 1,05 TO WRK-AUMENTO
             WHEN 6 THRU 16
               MOVE 1,10 TO WRK-AUMENTO
             WHEN ANY
               MOVE 1,15 TO WRK-AUMENTO
           END-EVALUATE.

           IF WRK-ANO-ENTRADA > 0 AND WRK-SALARIO > 0
             DISPLAY 'TEMPO TRABALHADO: ' WRK-ANO
             COMPUTE WRK-AUMENTO = WRK-SALARIO * WRK-AUMENTO
             DISPLAY '--------------------------'
             DISPLAY 'NOVO SALARIO: ' WRK-AUMENTO
           ELSE
             DISPLAY 'FALTA INFORMAR ALGO'
           END-IF.

           STOP RUN.
