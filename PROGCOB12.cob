       IDENTIFICATION DIVISION.
       PROGRAM-ID. PROGCOB12.
      *********************************
      * AREA DE COMENTARIOS - REMARKS
      * AUTHOR = Marcelo
      * DATA = 15/11/2019
      * OBJETIVO = RECEBER 02 NOTAS, MEDIA E IMPRIMIR
      * UTILIZAR PROGRAMACAO ESTRUTURADA
      *********************************
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       77 WRK-NOTA1     PIC 9(2)    VALUE ZEROS.
       77 WRK-NOTA2     PIC 9(2)    VALUE ZEROS.
       77 WRK-MEDIA     PIC 9(2)V99 VALUE ZEROS.
       77 WRK-MEDIA-ED  PIC Z9,99   VALUE ZEROS.
       PROCEDURE DIVISION.
       0001-PRINCIPAL.
           PERFORM 0100-INICIALIZAR.
           IF WRK-NOTA1 > 0 AND WRK-NOTA2 > 2
               PERFORM 0200-PROCESSAR
               PERFORM 0300-FINALIZAR
           END-IF.
           STOP RUN.
       0100-INICIALIZAR.
           ACCEPT WRK-NOTA1.
           ACCEPT WRK-NOTA2.
       0200-PROCESSAR.
           COMPUTE WRK-MEDIA = (WRK-NOTA1 + WRK-NOTA2) / 2.
           MOVE WRK-MEDIA TO WRK-MEDIA-ED.
           DISPLAY 'MEDIA ' WRK-MEDIA-ED.

           IF WRK-MEDIA >= 6,00
             DISPLAY 'APROVADO'
           ELSE
             IF WRK-MEDIA >= 2,00
               DISPLAY 'RECUPERACAO'
             ELSE
               DISPLAY 'REPROVADO'
             END-IF
           END-IF.
       0300-FINALIZAR.
           DISPLAY '--------------------------'.
           DISPLAY 'FINAL DE PROCESSAMENTO'.
