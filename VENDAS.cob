       IDENTIFICATION DIVISION.
       PROGRAM-ID. VENDAS.
      *********************************
      * AREA DE COMENTARIOS - REMARKS
      * AUTHOR = Marcelo
      * DATA = 11/11/2019
      * OBJETIVO = VENDAS
      *********************************
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       77 WRK-VENDA1   PIC 9(3)V99   VALUE ZEROS.
       77 WRK-VENDA2   PIC 9(3)V99   VALUE ZEROS.
       77 WRK-MEDIA    PIC 9(4)      VALUE ZEROS.
       77 WRK-MEDIA-ED PIC $Z.ZZ9,99 VALUE ZEROS.
       PROCEDURE DIVISION.
           ACCEPT WRK-VENDA1 FROM CONSOLE.
           ACCEPT WRK-VENDA2 FROM CONSOLE.
           DISPLAY '==================================='.
           DISPLAY 'VENDA1.. ' WRK-VENDA1.
           DISPLAY 'VENDA2.. ' WRK-VENDA2.

           ADD WRK-VENDA1 WRK-VENDA2 TO WRK-MEDIA.
           DIVIDE WRK-MEDIA BY 2 GIVING WRK-MEDIA.
           MOVE WRK-MEDIA TO WRK-MEDIA-ED.
           DISPLAY 'MEDIA.. ' WRK-MEDIA-ED.
           STOP RUN.
