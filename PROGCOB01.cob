       IDENTIFICATION DIVISION.
       PROGRAM-ID. PROGCOB01.
      *********************************
      * AREA DE COMENTARIOS - REMARKS
      * AUTHOR = Marcelo
      * DATA = 06/11/2019
      * OBJETIVO = MOSTRAR A STRING HELLO ALURA
      *********************************
       ENVIRONMENT DIVISION.
       DATA DIVISION.
       PROCEDURE DIVISION.
           DISPLAY "HELLO ALURA".
           STOP RUN.
